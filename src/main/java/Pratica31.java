/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * PG    - Câmpus de Ponta Grossa
 * DAFIS - Departamento Acadêmico de Física
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Abel Dionizio Azeredo <aazeredo@utfpr.edu.br>
 */

import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
        
public class Pratica31 {
    public static void main(String[] args) {
      
        Date inicio = new Date();
        String meuNome = "abel dionizio azeREdo";
        System.out.println(meuNome.toUpperCase());
        System.out.println(meuNome.substring(14,15).toUpperCase() + meuNome.substring(15).toLowerCase() +", "+ meuNome.substring(0,1).toUpperCase() +". "+ meuNome.substring(5,6).toUpperCase() +".");
        GregorianCalendar dataNascimento = new GregorianCalendar(1971, Calendar.NOVEMBER, 22);
        Long tempo_desde_o_nascimento = inicio.getTime() - dataNascimento.getTime().getTime();
        Long dias = tempo_desde_o_nascimento/1000/60/60/24;
        
                int dia = dataNascimento.get(Calendar.DAY_OF_MONTH);
                int mes = dataNascimento.get(Calendar.MONTH) + 1;
                int ano = dataNascimento.get(Calendar.YEAR);
                System.out.println("Dias decorridos desde o meu nascimento ("+ dia +"/"+ mes +"/"+ ano +") até hoje: "+ dias);
                
        Date fim = new Date();
        Long tempo = fim.getTime() - inicio.getTime();
        System.out.println("Tempo de execução decorrido = "+tempo +" milissegundos");
    }
}
